﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VectorGraphicViewer.Shapes
{
    public class Triangle : Shape
    {
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string Filled {get; set;}

        public Triangle(Dictionary<string, string> shapeDataObject) : base(shapeDataObject)
        {
            this.A = shapeDataObject["a"];
            this.B = shapeDataObject["b"];
            this.C = shapeDataObject["c"];

            this.Filled = shapeDataObject["filled"];
        }

        public override string GetShapeHtml(double offsetX, double offsetY, double interval, double tickCount, double maxValue)
        {
            string[] a = A.Split(';');
            string[] b = B.Split(';');
            string[] c = C.Split(';');

            string bx = (offsetX + double.Parse(b[0].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");
            string by = (offsetY - double.Parse(b[1].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");
            string ay = (offsetY - double.Parse(a[1].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");
            string ax = (offsetX + double.Parse(a[0].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");
            string cy = (offsetY - double.Parse(c[1].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");
            string cx = (offsetX + double.Parse(c[0].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");

            string style = GetShapeStyle();
            string strokeDasharray = GetLineTypeHtml();

            string html = "<path d=\"" + "M" + ax + " " + ay + " L" + bx + " " + by + " L" + cx + " " + cy + " Z\" "+strokeDasharray+" style=\""+style+ "\"   class=\"shape\" >"+GetShapeInfoJSON()+"</path>";


            return html;
        }

        private string GetShapeStyle()
        {
            string style = GetBaseShapeStyle() +(Filled == "true" ? " fill : rgba(" + GetColorCSS() + ")" : " fill : none; ");

            return style;
        }

        public override double GetMaxValue()
        {
            string[] a = A.Split(';');
            string[] b = B.Split(';');
            string[] c = C.Split(';');

            double[] arr = { Math.Abs(double.Parse(b[0].Trim())),
                             Math.Abs(double.Parse(b[1].Trim())),
                             Math.Abs(double.Parse(a[1].Trim())),
                             Math.Abs(double.Parse(a[0].Trim())),
                             Math.Abs(double.Parse(c[1].Trim())),
                             Math.Abs(double.Parse(c[0].Trim()))
                            };

            return arr.Max();
        }
    }
}