﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VectorGraphicViewer.Shapes
{
    public class Circle:Shape
    {
        public string Center { get; set; }
        public string Radius { get; set; }
        public string Filled { get; set; }

        public Circle(Dictionary<string, string> shapeDataObject): base(shapeDataObject)
        {            
            this.Center = shapeDataObject["center"];
            this.Radius = shapeDataObject["radius"];
            this.Filled = shapeDataObject["filled"];
        }

        public override string GetShapeHtml(double offsetX, double offsetY, double interval, double tickCount, double maxValue)
        {
            string[] center = Center.Split(';');
            string radius = Radius;

            string r = ((double.Parse(radius.Trim().Replace(".", ",")) / 2) * interval * tickCount / maxValue).ToString().Replace(",", ".");
            string cx = (offsetX + double.Parse(center[0].Trim()) * interval * tickCount / maxValue).ToString().Replace(",", ".");
            string cy = (offsetY - double.Parse(center[1].Trim()) * interval * tickCount / maxValue).ToString().Replace(",", ".");

            string style = GetShapeStyle();
            string strokeDasharray = GetLineTypeHtml();

            string html = "<circle cx=\"" + cx + "\" cy=\"" + cy + "\" r=\"" + r + "\"  " + strokeDasharray + " style=\"" + style+ "\"   class=\"shape\" >" + GetShapeInfoJSON() + "</circle>";

            return html;
        }

        private string GetShapeStyle()
        {
            string style = GetBaseShapeStyle() + (Filled == "true" ? " fill : rgba(" + GetColorCSS() + ")" : "fill :none; ");

            return style;
        }

        public override double GetMaxValue()
        {
            string[] center = Center.Split(';');
            double radius = double.Parse(Radius.Trim().Replace(".",","));

            double[] arr = { Math.Abs(radius + double.Parse(center[0].Trim())), Math.Abs(radius + double.Parse(center[1].Trim())) };

            return arr.Max();
        }

    }
}