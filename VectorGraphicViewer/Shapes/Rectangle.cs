﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VectorGraphicViewer.Shapes
{
    public class Rectangle : Shape
    {
        public string A { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Filled { get; set; }

        public Rectangle(Dictionary<string, string> shapeDataObject) : base(shapeDataObject)
        {
            this.A = shapeDataObject["a"];
            this.Width = shapeDataObject["width"];
            this.Height = shapeDataObject["height"];
            this.Filled = shapeDataObject["filled"];
        }

        public override string GetShapeHtml(double offsetX, double offsetY, double interval, double tickCount, double maxValue)
        {
            string[] a = A.Split(';');

            string width = ((double.Parse(Width.Trim().Replace(".", ",")) ) * interval * tickCount / maxValue).ToString().Replace(",", ".");
            string height = ((double.Parse(Height.Trim().Replace(".", ","))) * interval * tickCount / maxValue).ToString().Replace(",", ".");

            string x = (offsetX + double.Parse(a[0].Trim()) * interval * tickCount / maxValue).ToString().Replace(",", ".");
            string y = (offsetY - double.Parse(a[1].Trim()) * interval * tickCount / maxValue).ToString().Replace(",", ".");

            string style = GetShapeStyle();
            string strokeDasharray = GetLineTypeHtml();

            string html = "<rect x=\"" + x + "\" y=\"" + y + "\" width=\"" + width + "\" height=\"" + height + "\" " + strokeDasharray + " style=\"" + style + "\"   class=\"shape\" >" + GetShapeInfoJSON() + "</rectangle>";

            return html;
        }

        private string GetShapeStyle()
        {
            string style = GetBaseShapeStyle() + (Filled == "true" ? " fill : rgba(" + GetColorCSS() + ")" : "fill :none; ");

            return style;
        }

        public override double GetMaxValue()
        {
            string[] a = A.Split(';');
            double width = double.Parse(Width.Trim());
            double height = double.Parse(Height.Trim());

            double[] arr = { width + Math.Abs(double.Parse(a[0].Trim())), height + Math.Abs(double.Parse(a[1].Trim())) };

            return arr.Max();
        }
    }
}