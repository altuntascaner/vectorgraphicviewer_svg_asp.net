﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VectorGraphicViewer.Shapes
{
    public class Line : Shape
    {
        public string A { get; set; }
        public string B { get; set; }
        
        public Line(Dictionary<string,string> shapeDataObject):base(shapeDataObject)
        {            
            this.A = shapeDataObject["a"];
            this.B = shapeDataObject["b"];
        }

        public override string GetShapeHtml(double offsetX, double offsetY, double interval, double tickCount, double maxValue)
        {
            string[] a = A.Split(';');
            string[] b = B.Split(';');

            string x2 = (offsetX + double.Parse(b[0].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");
            string y2 = (offsetY - double.Parse(b[1].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");
            string y1 = (offsetY - double.Parse(a[1].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");
            string x1 = (offsetX + double.Parse(a[0].Trim()) * interval * 10 / maxValue).ToString().Replace(",", ".");

            string style = base.GetBaseShapeStyle();
            string strokeDasharray = GetLineTypeHtml();

            string html = "<line x1 = \"" + x1 + "\" y1 = \"" + y1 + "\" x2 = \"" + x2 + "\" y2 = \"" + y2 + "\" " + strokeDasharray + " style = \"" + style+ "\"  class=\"shape\" >" + GetShapeInfoJSON() + "</line>";

            return html;
        }

        public override double GetMaxValue()
        {
            string[] a = A.Split(';');
            string[] b = B.Split(';');

            double[] arr = { Math.Abs(double.Parse(b[0].Trim())), Math.Abs(double.Parse(b[1].Trim())), Math.Abs(double.Parse(a[1].Trim())), Math.Abs(double.Parse(a[0].Trim())) };

            return arr.Max();
        }

    }
}