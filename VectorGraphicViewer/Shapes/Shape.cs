﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace VectorGraphicViewer.Shapes
{
    public abstract class Shape
    {
        public string Type { get; set; }
        public string Color { get; set; }
        public string LineType { get; set; }
        public Dictionary<string,string> ShapeDataObject { get; set; }

        public Shape(Dictionary<string, string> shapeDataObject)
        {
            this.Type = shapeDataObject["type"];
            this.Color = shapeDataObject["color"];
            this.LineType = shapeDataObject["lineType"];
            this.ShapeDataObject = shapeDataObject;
        }

        public virtual string GetShapeHtml(double offsetX, double offsetY, double interval, double tickCount, double maxValue)
        {
            return "";
        }

        public string GetBaseShapeStyle()
        {
            string color = GetColorCSS();

            string style = " stroke: rgba("+ color +");"
                            +"stroke-width: 5;";

            return style;
        }

        public string GetColorCSS()
        {
            List<string> temp = Color.Split(';').Select(x => x.Replace(",", ".").Trim()).ToList();

            double alpha = double.Parse(temp[0]);
            temp.Add( (alpha / 255).ToString().Replace(",",".") );
            temp.RemoveAt(0);

            string color = String.Join(",", temp);

            return color;
        }

        public string GetLineTypeHtml()
        {
            string dashArray = " stroke-linecap=\"round\" ";

            if (LineType == "dot")
            {
                dashArray += " stroke-dasharray = \"1,10\" ";
            }
            else if (LineType == "dashDot")
            {
                dashArray += " stroke-dasharray = \"1,10,15,10\" ";
            }

            return dashArray;
        }

        public virtual string GetShapeInfoJSON()
        {
            return JsonConvert.SerializeObject(ShapeDataObject);
        }

        public virtual double GetMaxValue()
        {
            return 0;
        }
    }
}