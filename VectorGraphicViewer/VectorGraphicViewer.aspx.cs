﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web.Services;
using VectorGraphicViewer.Shapes;
using Newtonsoft.Json;
using System.Xml;

namespace VectorGraphicViewer
{
    public partial class VectorGraphicViewer : System.Web.UI.Page
    {
        static double AXIS_ARROW_OFFSET = 5;
        static double TICK_LENGTH = 5;
        static double TICK_COUNT = 10;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string DrawCoordinateSystem(string maxValue, string width, string height)
        {
            double windowHeight = double.Parse(height),
                   windowWidth = double.Parse(width),
                   maxVal = double.Parse(maxValue);

            string html = DrawAxes(windowWidth, windowHeight) + DrawTicks(maxVal, windowWidth, windowHeight);

            return html;
        }

        protected void btnDraw_Click(object sender, EventArgs e)
        {
            string errorMessage = "", svgContent = "";

            List<Dictionary<string, string>> shapeData = null;

            try
            {
                shapeData = GetDataFromFile();
            }
            catch (Exception e1)
            {
                errorMessage = "File could not be parsed. Exception : " + e1.Message;
            }

            if (shapeData != null)
            {
                List<Shape> shapes = new List<Shape>();

                Shape shapeItem = null;

                double maxValue = 0, shapeMaxValue = 0;

                int i = 0;

                try
                {
                    Dictionary<string, string> item;

                    for (i = 0; i < shapeData.Count; i++)
                    {
                        item = shapeData[i];

                        if (item["type"] == "line")
                        {
                            shapeItem = new Line(item);
                        }
                        else if (item["type"] == "circle")
                        {
                            shapeItem = new Circle(item);
                        }
                        else if (item["type"] == "triangle")
                        {
                            shapeItem = new Triangle(item);
                        }
                        else if (item["type"] == "rectangle")
                        {
                            shapeItem = new Rectangle(item);
                        }

                        shapes.Add(shapeItem);

                        shapeMaxValue = shapeItem.GetMaxValue();

                        if (shapeMaxValue > maxValue)
                        {
                            maxValue = shapeMaxValue;
                        }
                    }
                }
                catch
                {
                    errorMessage = "An error occured while creating the shape object. Please check shape data : " + JsonConvert.SerializeObject(shapeData[i]);
                }

                try
                {
                    maxValue = maxValue > 5 ? 10 * Math.Ceiling(maxValue / TICK_COUNT) : maxValue;

                    double WINDOW_HEIGHT = int.Parse(windowHeight.Value);
                    double WINDOW_WIDTH = int.Parse(windowWidth.Value);

                    double midPointX = Math.Floor(WINDOW_WIDTH / 2);
                    double midPointY = Math.Floor(WINDOW_HEIGHT / 2);

                    double interval = (midPointY - 4 * AXIS_ARROW_OFFSET) / TICK_COUNT;

                    svgContent = DrawCoordinateSystem(maxValue.ToString(), WINDOW_WIDTH.ToString(), WINDOW_HEIGHT.ToString());

                    svgContent += string.Join("", shapes.Select(x => x.GetShapeHtml(midPointX, midPointY, interval, TICK_COUNT, maxValue)));
                }
                catch
                {
                    errorMessage = "An error occured while drawing. Plase contact to admin";
                }

                
            }

            Label1.Value = "{\"fileName\":\"" + fileUploader.FileName + "\",\"errorMessage\":\"" + errorMessage + "\",\"svgContent\" :\"" + svgContent.Replace("\"", "\\\"") + "\"}";
        }

        List<Dictionary<string, string>> GetDataFromFile()
        {
            StreamReader reader = new StreamReader(fileUploader.FileContent);

            string fileContent = reader.ReadToEnd();

            string extension = fileUploader.FileName.Substring(fileUploader.FileName.LastIndexOf('.')).ToLower();

            List<Dictionary<string, string>> shapeData = null;

            if (extension == ".json")
            {
                shapeData = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(fileContent);
            }
            else if (extension == ".xml")
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(fileContent);

                string json = JsonConvert.SerializeXmlNode(xmlDoc.LastChild).Replace("{\"root\":{\"shape\":", "");
                json = json.Substring(0, json.Length - 2);
                json = json.Contains("[{") ? json : "[" + json + "]";

                shapeData = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(json);
            }

            return shapeData;
        }

        static string DrawAxes(double windowWidth, double windowHeight)
        {
            var midPointX = Math.Floor(windowWidth / 2);
            var midPointY = Math.Floor(windowHeight / 2);


            var yAxisHtml = "<line x1 = \"" + midPointX + "\" y1 = \"0\" x2 = \"" + (midPointX - AXIS_ARROW_OFFSET) + "\" y2 = \"" + AXIS_ARROW_OFFSET + "\" class = \"axisLine\"  />"
                + "<line x1 = \"" + midPointX + "\" y1 = \"0\" x2 = \"" + (midPointX + AXIS_ARROW_OFFSET) + "\" y2 = \"" + AXIS_ARROW_OFFSET + "\" class = \"axisLine\" />"
                + "<line x1 = \"" + midPointX + "\" y1 = \"0\" x2 = \"" + midPointX + "\" y2 = \"" + windowHeight + "\" class=\"axisLine\" />"
                + "<line x1 = \"" + midPointX + "\" y1 = \"" + windowHeight + "\" x2 = \"" + (midPointX - AXIS_ARROW_OFFSET) + "\" y2 = \"" + (windowHeight - AXIS_ARROW_OFFSET) + "\" class=\"axisLine\" />"
                + "<line x1 = \"" + midPointX + "\" y1 = \"" + windowHeight + "\" x2 = \"" + (midPointX + AXIS_ARROW_OFFSET) + "\" y2 = \"" + (windowHeight - AXIS_ARROW_OFFSET) + "\" class=\"axisLine\" />";

            var xAxisHtml = "<line x1 = \"0\" y1 = \"" + midPointY + "\" x2 = \"" + AXIS_ARROW_OFFSET + "\" y2 = \"" + (midPointY - AXIS_ARROW_OFFSET) + "\" class = \"axisLine\"  />"
                + "<line x1 = \"0\" y1 = \"" + midPointY + "\" x2 = \"" + AXIS_ARROW_OFFSET + "\" y2 = \"" + (midPointY + AXIS_ARROW_OFFSET) + "\" class = \"axisLine\" />"
                + "<line x1 = \"0\" y1 = \"" + midPointY + "\" x2 = \"" + windowWidth + "\" y2 = \"" + midPointY + "\" class=\"axisLine\" />"
                + "<line x1 = \"" + windowWidth + "\" y1 = \"" + midPointY + "\" x2 = \"" + (windowWidth - AXIS_ARROW_OFFSET) + "\" y2 = \"" + (midPointY - AXIS_ARROW_OFFSET) + "\" class=\"axisLine\" />"
                + "<line x1 = \"" + windowWidth + "\" y1 = \"" + midPointY + "\" x2 = \"" + (windowWidth - AXIS_ARROW_OFFSET) + "\" y2 = \"" + (midPointY + AXIS_ARROW_OFFSET) + "\" class=\"axisLine\" />";

            return yAxisHtml + xAxisHtml;
        }

        static string DrawTicks(double maxValue, double windowWidth, double windowHeight)
        {
            var midPointX = Math.Floor(windowWidth / 2);
            var midPointY = Math.Floor(windowHeight / 2);

            string ticksHtml = "", tempText, tempIndex;

            var interval = (midPointY - 4 * AXIS_ARROW_OFFSET) / TICK_COUNT;

            maxValue = maxValue > 5 ? TICK_COUNT * Math.Ceiling(maxValue / TICK_COUNT) : maxValue;

            var tickStep = maxValue / TICK_COUNT;

            var maxValueX = maxValue * (midPointX - -4 * AXIS_ARROW_OFFSET) / (midPointY - -4 * AXIS_ARROW_OFFSET);

            for (var i = tickStep; i <= maxValueX; i += tickStep)
            {
                tempIndex = (midPointX + (i / tickStep) * interval).ToString().Replace(",", ".");
                tempText = Math.Round(i, 2).ToString().Replace(".00", "");

                ticksHtml += "<line x1=\"" + tempIndex + "\" y1=\"" + (midPointY - TICK_LENGTH) + "\" x2=\"" + tempIndex + "\" y2=\"" + (midPointY + TICK_LENGTH) + "\" class=\"axisLine\" />"
                    + "<line x1=\"" + tempIndex + "\" y1=\"0\" x2=\"" + tempIndex + "\" y2=\"" + windowHeight + "\" class=\"gridLine\" />"
                    + "<text x=\"" + tempIndex + "\" y=\"" + (midPointY + 15) + "\" class=\"tickText\">" + tempText + "</text>";

                tempIndex = (midPointX - (i / tickStep) * interval).ToString().Replace(",", ".");

                ticksHtml += "<line x1=\"" + tempIndex + "\" y1=\"" + (midPointY - TICK_LENGTH) + "\" x2=\"" + tempIndex + "\" y2=\"" + (midPointY + TICK_LENGTH) + "\" class=\"axisLine\" />"
                    + "<line x1=\"" + tempIndex + "\" y1=\"0\" x2=\"" + tempIndex + "\" y2=\"" + windowHeight + "\" class=\"gridLine\" />"
                    + "<text x=\"" + tempIndex + "\" y=\"" + (midPointY - 14) + "\" class=\"tickText\">-" + tempText + "</text>";
            }

            tickStep = maxValue / TICK_COUNT;

            for (var i = tickStep; i <= maxValue; i += tickStep)
            {
                tempIndex = (midPointY + (i / tickStep) * interval).ToString().Replace(",", ".");
                tempText = Math.Round(i, 2).ToString().Replace(".00", "");

                ticksHtml += "<line x1=\"" + (midPointX - TICK_LENGTH) + "\" y1=\"" + tempIndex + "\" x2=\"" + (midPointX + TICK_LENGTH) + "\" y2=\"" + tempIndex + "\" class=\"axisLine\" />"
                    + "<line x1=\"0\" y1=\"" + tempIndex + "\" x2=\"" + windowWidth + "\" y2=\"" + tempIndex + "\" class=\"gridLine\" />"
                    + "<text x=\"" + (midPointX + 10) + "\" y=\"" + tempIndex + "\" class=\"tickText\">-" + tempText + "</text>";

                tempIndex = (midPointY - (i / tickStep) * interval).ToString().Replace(",", ".");

                ticksHtml += "<line x1=\"" + (midPointX - TICK_LENGTH) + "\" y1=\"" + tempIndex + "\" x2=\"" + (midPointX + TICK_LENGTH) + "\" y2=\"" + tempIndex + "\" class=\"axisLine\" />"
                    + "<line x1=\"0\" y1=\"" + tempIndex + "\" x2=\"" + windowWidth + "\" y2=\"" + tempIndex + "\" class=\"gridLine\" />"
                    + "<text x=\"" + (midPointX + 10) + "\" y=\"" + tempIndex + "\" class=\"tickText\">" + tempText + "</text>";
            }

            return ticksHtml;
        }
    }
}