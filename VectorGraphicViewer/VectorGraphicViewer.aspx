﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VectorGraphicViewer.aspx.cs" Inherits="VectorGraphicViewer.VectorGraphicViewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vector Graphic Viewer</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script type="text/javascript">
        var WINDOW_WIDTH;
        var WINDOW_HEIGHT;

        var AXIS_ARROW_OFFSET = 5;
        var TICK_LENGTH = 5;
        var TICK_COUNT = 10;

        $(document).ready(function () {
            WINDOW_WIDTH = Math.floor($('#svgContainer').parent().width() - $('#svgContainer').parent().width() * 0.02); //Math.floor($(window).width() - $(window).width() * 0.03);
            WINDOW_HEIGHT = Math.floor($(window).height() - $(window).height() * 0.04);

            $('#windowHeight').val(WINDOW_HEIGHT);
            $('#windowWidth').val(WINDOW_WIDTH);

            $('#svgContainer').width(WINDOW_WIDTH);
            $('#svgContainer').height(WINDOW_HEIGHT);

            var res = $('#Label1').val();

            if (!IsNullOrEmpty(res)) {

                var drawResult = JSON.parse(res);

                var fileName = drawResult.fileName;

                $('#spanSelectedFileName').html(fileName);
                $('#divFileName').show();

                var errorMessage = drawResult.errorMessage;
                var svgContent = drawResult.svgContent;

                if (!IsNullOrEmpty(errorMessage)) {
                    alert(errorMessage);
                }
                else if (!IsNullOrEmpty(svgContent)) {
                    $('#svgContainer').html(svgContent);
                    $('#divShapeInfoContainer').show();
                }

                $('#Label1').val('');
            }
            else {
                var coordSys = DrawCoordinateSystem(50, WINDOW_WIDTH, WINDOW_HEIGHT);

                $('#svgContainer').html(coordSys);
            }

            $('#svgContainer').html($('#svgContainer').html());

            $('#fileUploader').change(function () {
                var fileName = $(this)[0].files[0].name;

                if (CheckFileExtension(fileName)) {
                    $('#spanSelectedFileName').html(fileName);
                    $('#divFileName').show();
                    $('#btnDraw').show();
                }
                else {
                    $('#divFileName').hide();
                    $('#btnDraw').hide();

                    alert('Only json and xml files can be loaded.');
                }
            });

            var temp;

            $(document).on('mouseenter', '.shape', function () {

                $(this).css('stroke-width', '7');
                temp = $(this).css('stroke');
                $(this).css('stroke', 'green');

            }).on('mouseleave', '.shape', function () {

                $(this).css('stroke-width', '5');
                $(this).css('stroke', temp);

            }).on('click', '.shape', function () {
                var dataObj = JSON.parse($(this).html());

                var html = '';

                for (var key in dataObj) {
                    html += '<div><span class="infoTitle">' + key + '</span><span > : </span>' + dataObj[key] + '</div>';
                }

                $('#divShapeInfo').html(html);
                });

            $(window).resize(function () {
                $('#windowHeight').val(Math.floor($(window).height() - $(window).height() * 0.04));
                $('#windowWidth').val(Math.floor($('#svgContainer').parent().width() - $('#svgContainer').parent().width() * 0.02));
            });
        });

        function DrawCoordinateSystem(maxValue, width, height) {
            var html;

            $.ajax({
                method: 'POST',
                url: 'VectorGraphicViewer.aspx/DrawCoordinateSystem',
                data: JSON.stringify({ maxValue: maxValue, width: width, height: height }),
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                success: function (result) {
                    html = result.d;
                }
            });

            return html;
        }

        function CheckFileExtension(uploadedFileName) {
            try {
                var re = /\..+$/;
                var ext = uploadedFileName.match(re)[0];

                if (ext.toLowerCase() == '.json' || ext.toLowerCase() == '.xml') {
                    return true;
                } else {
                    return false;
                }
            } catch (e) {
                return false;
            }
        }

        function IsNullOrEmpty(val) {
            return val == null || val === undefined || val == '';
        }

    </script>

    <style>
        .axisLine {
            stroke: black;
            stroke-width: 2;
        }

        .gridLine {
            stroke: gray;
            stroke-width: 1;
            stroke-dasharray: 3;
        }

        .tickText {
            fill: red;
            font-size: 10px;
            font-weight: bolder;
            z-index: 1;
        }

        .shape {
            cursor: pointer;
        }

        a.linkSampleFile {
            text-decoration: none;
            cursor: pointer !important;
            color: blue;
        }

            a.linkSampleFile:hover {
                color: orange;
            }

        .menuButton:hover {
            background-color: orange;
        }

        .menuButton {
            color: black;
            font-size: 17px;
            border-radius: 5px; /* box-shadow: 0px 0px 4px 1px grey; */
            border-style: groove;
            border-width: 3px;
            cursor: pointer;
            outline: none;
        }

        #divShapeInfo span {
            font-weight: bolder;
        }

        span.infoTitle {
            display: inline-block;
            width: 25%;
        }
    </style>
</head>

<body style="margin: 0">

    <div style="float: left; width: 15%; padding: 0.5%; box-shadow: 0px 0px 13px 0px grey; margin-top: 0.5%; margin-left: 0.5%;">

        <div style="margin-bottom: 10%;">
            <span style="font-weight: bold; margin-right: 10px">Sample File </span>
            <a href="SampleFiles/importShape.json" target="_blank" download="SampleJSON.json" class="linkSampleFile">JSON</a> - 
            <a href="SampleFiles/importShape.xml" target="_blank" download="SampleXML.xml" class="linkSampleFile">XML</a>
        </div>
        <form id="form1" runat="server">
            <div>
                <span style="font-weight: bold; margin-right: 10px">Import File </span>
                <input type="button" value="Browse" id="btnBrowse" onclick="$('#fileUploader').click()" class="menuButton" />
                <asp:Button ID="btnDraw" runat="server" Text="Draw" OnClick="btnDraw_Click" class="menuButton" Style="display: none" />
                <br />
                <br />
                <div id="divFileName" style="display: none">
                    <span style="font-weight: bold; margin-right: 10px;">File Name : </span>
                    <span id="spanSelectedFileName" style="word-break: break-all;"></span>
                </div>

                <asp:FileUpload ID="fileUploader" runat="server" Style="display: none" />

                <asp:HiddenField ID="Label1" runat="server" />
                <asp:HiddenField ID="windowHeight" runat="server" />
                <asp:HiddenField ID="windowWidth" runat="server" />
            </div>
        </form>
        <br />
        <div id="divShapeInfoContainer" style="display: none">

            <span style="font-weight: bold; ">Shape Information</span>

            <div id="divShapeInfo" style="margin-top: 3%;">
                Click on a shape to see info.
            </div>
        </div>
    </div>

    <div style="float: left; width: 80%; padding: 0.5%;">
        <svg id="svgContainer" style="margin-left: 1%; margin-top: 1%">
        </svg>
    </div>
</body>
</html>
